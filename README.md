# Meta fig workshop 1st of February 2018, Liege

Meta-fig is part of a series of workshops around the practice and processes using metapost called meta worshops.
During the Fig-festival of Liège, OSP proposed a workshop using matapost, F/Loss software, to draw letters from the stroke in a collective and parametric way.
http://figliege.meta.fr/

* OSP organised other workshops on matapost during the last years  </br>
[osp.kitchen/workshop/metahoguet](http://osp.kitchen/workshop/metahoguet)  </br>
[osp.kitchen/workshop/meta-brasil](http://osp.kitchen/workshop/meta-brasil)  </br>
[osp.kitchen/workshop/meta-port-au-prince](http://osp.kitchen/workshop/meta-port-au-prince)  

* General concept and tools for those metapost workshops  
[osp.kitchen/workshop/metapost-workshops](http://osp.kitchen/workshop/metapost-workshops)

## How to install Metapost

→ [osp.kitchen/workshop/metapost-workshops](http://osp.kitchen/workshop/metapost-workshops)